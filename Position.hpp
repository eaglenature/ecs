#pragma once
#include "ComponentData.hpp"

struct Position : ComponentData
{
    float x;
    float y;
    float z;

    static constexpr UniqueId GetUniqueId() { return UniqueId::Position; }
};
