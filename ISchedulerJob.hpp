#pragma once

#include <string>

struct ISchedulerJob
{
    virtual ~ISchedulerJob() = default;

    virtual void execute() = 0;
    virtual bool hasPredecessors() const = 0;
    virtual void addPredecessor() = 0;
    virtual void removePredecessor() = 0;
    virtual bool hasNext() const = 0;
    virtual void attachNext(ISchedulerJob* job) = 0;
    virtual ISchedulerJob* takeNext() = 0;

    virtual std::string getName() const = 0;
};
