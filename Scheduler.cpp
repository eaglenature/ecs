#include "ITaskRunner.hpp"
#include "ISchedulerJob.hpp"
#include "Scheduler.hpp"

Scheduler::Scheduler(ITaskRunner& taskRunner)
    : taskRunner(taskRunner)
{}

void Scheduler::schedule(ISchedulerJob* job)
{
    if (not job->hasPredecessors())
    {
        taskRunner.submit([this, job]()
        {
            job->execute();
            if (job->hasNext())
            {
                this->scheduleNext(job);
                return;
            }
            this->notifyDone();
        });
    }
}

void Scheduler::scheduleNext(ISchedulerJob* job)
{
    auto next = job->takeNext();
    schedule(next);
}

void Scheduler::waitUntilDone()
{
    std::unique_lock<std::mutex> guard{m};
    conditionDone.wait(guard, [this]{ return done; });
}

void Scheduler::notifyDone()
{
    std::unique_lock<std::mutex> guard{m};
    done = true;
    conditionDone.notify_one();
}

void Scheduler::addJob(ISchedulerJob* job)
{
    jobs.push_back(job);
}

void Scheduler::schedule()
{
    done = false;
    for (auto job : jobs)
    {
        schedule(job);
    }
    waitUntilDone();
}
