#pragma once
#include "ComponentData.hpp"

struct Velocity : ComponentData
{
    float value;

    static constexpr UniqueId GetUniqueId() { return UniqueId::Velocity; }
};
