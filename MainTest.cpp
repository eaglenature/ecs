#include <gtest/gtest.h>
#include "Position.hpp"
#include "Rotation.hpp"
#include "Velocity.hpp"
#include "ComponentSystem.hpp"
#include "ProcessComponentData.hpp"
#include "EntityManager.hpp"

using namespace ::testing;

namespace
{
struct MainTest : Test
{};
}

struct RotationProcess : ProcessComponentData<Position, Rotation>
{
    void execute()
    {
    }
};

struct MovementProcess : ProcessComponentData<Velocity, Position, Rotation>
{
    void execute()
    {
    }
};

TEST_F(MainTest, testRotationProcess)
{
    RotationProcess rp;
    {
        auto& positions = rp.queryData<Position>();
        ASSERT_EQ(100u, positions.size());

        auto& rotations = rp.queryData<Rotation>();
        ASSERT_EQ(200u, rotations.size());
    }

    MovementProcess mp;
    {
        auto& positions = mp.queryData<Position>();
        ASSERT_EQ(100u, positions.size());

        auto& rotations = mp.queryData<Rotation>();
        ASSERT_EQ(200u, rotations.size());

        auto& velocities = mp.queryData<Velocity>();
        ASSERT_EQ(300u, velocities.size());
    }
}
