#include "ThreadPool.hpp"

ThreadPool::ThreadSafeQueue::ThreadSafeQueue(std::size_t queueLimit)
    : queueLimit(queueLimit)
{}

void ThreadPool::ThreadSafeQueue::push(ThreadPool::Job&& job)
{
    std::unique_lock<std::mutex> guard{m};
    conditionPush.wait(guard, [this](){ return jobs.size() < queueLimit; });
    jobs.push(std::move(job));
    conditionPop.notify_one();
}

ThreadPool::Job ThreadPool::ThreadSafeQueue::pop()
{
    std::unique_lock<std::mutex> guard{m};
    conditionPop.wait(guard, [this](){ return not jobs.empty(); });
    ThreadPool::Job job = std::move(jobs.front());
    jobs.pop();
    conditionPush.notify_one();
    return job;
}

ThreadPool::ThreadPool(std::size_t numOfThreads, std::size_t queueLimit)
    : queue(queueLimit)
{
    while (numOfThreads--) createWorker();
}

ThreadPool::~ThreadPool()
{
    for (auto i = 0u; i < workers.size(); ++i) queue.push(Job{});
    for (auto& worker : workers) worker.join();
}

void ThreadPool::submit(Task&& task)
{
    queue.push(Job{std::move(task)});
}

void ThreadPool::createWorker()
{
    workers.emplace_back([this]()
    {
        while (true)
        {
            Job job = queue.pop();
            if (not job.task) return;
            job.task();
        }
    });
}
