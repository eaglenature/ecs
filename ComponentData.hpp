#pragma once
#include <vector>
#include <memory>

struct ComponentData
{
    enum class UniqueId
    {
        Position,
        Rotation,
        Velocity
    };
};

template <typename ComponentData>
using ComponentDataArray = std::vector<ComponentData>;

struct Component
{
    using StoredType = std::unique_ptr<Component>;

    virtual ~Component() = default;
};

static std::size_t c = 0;

template <typename ComponentData>
class ComponentDataStore : public Component
{
public:
    ComponentDataStore(std::size_t numOfElements) : realData(numOfElements)
    {}

    static ComponentDataArray<ComponentData>& GetComponentDataArray(Component& component)
    {
        return static_cast<ComponentDataStore<ComponentData>&>(component).realData;
    }

    static ComponentDataArray<ComponentData> const& GetComponentDataArray(Component const& component)
    {
        return static_cast<ComponentDataStore<ComponentData> const&>(component).realData;
    }

    static std::unique_ptr<Component> CreateComponent()
    {
        c = c + 100;
        return std::make_unique<ComponentDataStore<ComponentData>>(c);
    }

private:
    ComponentDataArray<ComponentData> realData;
};
