#include <gtest/gtest.h>
#include <string>
#include <vector>
#include <iostream>
#include "CombinedRange.hpp"


namespace
{
struct CombinedRangeTest : ::testing::Test
{
};
}

TEST_F(CombinedRangeTest, shouldReadContentFromCombinedRanges)
{
    std::vector<int> values = {1, 2, 3, 4, 5};
    std::vector<std::string> cities = {"Helsinki", "Berlin", "Vancouver", "Tokyo", "Barcelona"};

    auto combinedRange = makeCombinedRange(values, cities);

    auto it = combinedRange.begin();
    ASSERT_EQ(1, std::get<0>(*it));
    ASSERT_EQ("Helsinki", std::get<1>(*it));

    ++it;
    ASSERT_EQ(2, std::get<0>(*it));
    ASSERT_EQ("Berlin", std::get<1>(*it));

    ++it;
    ASSERT_EQ(3, std::get<0>(*it));
    ASSERT_EQ("Vancouver", std::get<1>(*it));

    ++it;
    ASSERT_EQ(4, std::get<0>(*it));
    ASSERT_EQ("Tokyo", std::get<1>(*it));

    ++it;
    ASSERT_EQ(5, std::get<0>(*it));
    ASSERT_EQ("Barcelona", std::get<1>(*it));

    ++it;
    ASSERT_EQ(it, combinedRange.end());
}


TEST_F(CombinedRangeTest, shouldWriteToOriginalRanges)
{
    std::vector<int> values = {1, 2, 3, 4, 5};
    std::vector<std::string> cities = {"Helsinki", "Berlin", "Vancouver", "Tokyo", "Barcelona"};

    auto combinedRange = makeCombinedRange(values, cities);
    auto it = combinedRange.begin();

    std::get<0>(*it) = 100;
    std::get<1>(*it) = "New York";
    ASSERT_EQ(100, values[0]);
    ASSERT_EQ("New York", cities[0]);

    ++it;
    std::get<0>(*it) = 200;
    std::get<1>(*it) = "Los Angeles";
    ASSERT_EQ(200, values[1]);
    ASSERT_EQ("Los Angeles", cities[1]);

    ++it;
    std::get<0>(*it) = 300;
    std::get<1>(*it) = "Los Angeles";
    ASSERT_EQ(300, values[2]);
    ASSERT_EQ("Los Angeles", cities[2]);

    ++it;
    std::get<0>(*it) = 400;
    std::get<1>(*it) = "Los Angeles";
    ASSERT_EQ(400, values[3]);
    ASSERT_EQ("Los Angeles", cities[3]);

    ++it;
    std::get<0>(*it) = 500;
    std::get<1>(*it) = "Los Angeles";
    ASSERT_EQ(500, values[4]);
    ASSERT_EQ("Los Angeles", cities[4]);

    ++it;
    ASSERT_EQ(it, combinedRange.end());
}


namespace
{
template <typename Value>
struct TalkingClass
{
    static std::size_t instances;
    std::size_t a, b, c;
    Value value;
    TalkingClass() : a(instances++), b(a), c(a), value() { std::cout << __PRETTY_FUNCTION__ << " " << a << " " << b << " " << c << std::endl; }
    ~TalkingClass() { std::cout << __PRETTY_FUNCTION__ << " " << a << " " << b << " " << c << std::endl; }
    TalkingClass(TalkingClass&&) { std::cout << __PRETTY_FUNCTION__ << " " << a << " " << b << " " << c << std::endl; }
    TalkingClass(const TalkingClass&) { std::cout << __PRETTY_FUNCTION__ << " " << a << " " << b << " " << c << std::endl; }
    TalkingClass& operator=(const TalkingClass&) { std::cout << __PRETTY_FUNCTION__ << " " << a << " " << b << " " << c << std::endl; return *this; }
};
template <typename Value> std::size_t TalkingClass<Value>::instances = 0;

struct Double3 { double x, y, z; };

struct CombinedRangeWithTalkingClassTest : CombinedRangeTest
{
    void testIterationUsingIteratorsWithTalkingClass()
    {
        static constexpr auto NUM_OF_ELEMENTS = 4;
        std::vector<TalkingClass<int>> va{NUM_OF_ELEMENTS};
        std::vector<TalkingClass<std::string>> vb{NUM_OF_ELEMENTS};
        std::vector<TalkingClass<Double3>> vc{NUM_OF_ELEMENTS};
        std::cout << "-------------------------------------" << std::endl;
        auto combinedRange = makeCombinedRange(va, vb, vc);
        for (auto it = combinedRange.begin(); it != combinedRange.end(); ++it)
        {
            std::cout << std::get<0>(*it).a << " " << std::get<1>(*it).a << " " << std::get<2>(*it).a << std::endl;
        }
        std::cout << "-------------------------------------" << std::endl;
    }
    void testIterationUsingRangesWithTalkingClass()
    {
        static constexpr auto NUM_OF_ELEMENTS = 4;
        std::vector<TalkingClass<int>> va{NUM_OF_ELEMENTS};
        std::vector<TalkingClass<std::string>> vb{NUM_OF_ELEMENTS};
        std::vector<TalkingClass<Double3>> vc{NUM_OF_ELEMENTS};
        std::cout << "-------------------------------------" << std::endl;
        for (auto combinedItems : makeCombinedRange(va, vb, vc))
        {
            std::cout << std::get<0>(combinedItems).a << " " << std::get<1>(combinedItems).a << " " << std::get<2>(combinedItems).a << std::endl;
        }
        std::cout << "-------------------------------------" << std::endl;
    }
};
}

TEST_F(CombinedRangeWithTalkingClassTest, DISABLED_iterationUsingIterators)
{
    testIterationUsingIteratorsWithTalkingClass();
}

TEST_F(CombinedRangeWithTalkingClassTest, DISABLED_iterationUsingRangeForLoop)
{
    testIterationUsingRangesWithTalkingClass();
}
