# Makefile sample
TARGET = ecs

SOURCES := $(wildcard *.cpp)
OBJECTS := $(patsubst %.cpp,%.o,$(SOURCES))

CXX = g++

CXXFLAGS = -std=c++17 -c -Wall
LDFLAGS = -lgtest -lgtest_main -lgmock -lpthread

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CXX) -o $(TARGET) $(OBJECTS) $(LDFLAGS)

%.o : %.cpp
	@echo Compile: $<
	@$(CXX) $(CXXFLAGS) $<

clean:
	@echo Clean all binaries...
	@rm -f $(TARGET) $(OBJECTS)

.PHONY: all clean
