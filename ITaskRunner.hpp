#pragma once

#include <functional>

struct ITaskRunner
{
    using Task = std::function<void()>;
    virtual ~ITaskRunner() = default;
    virtual void submit(Task&&) = 0;
};
