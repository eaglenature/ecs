#pragma once
#include "EntityManager.hpp"

template <typename... Args>
struct ProcessComponentData
{
    ProcessComponentData()
    {
        (EntityManager::GetInstance().addComponent<Args>(), ...);
    }

    template <typename ComponentData>
    ComponentDataArray<ComponentData>& queryData()
    {
        return EntityManager::GetInstance().accessComponent<ComponentData>();
    }

    template <typename ComponentData>
    ComponentDataArray<ComponentData> const& queryData() const
    {
        return EntityManager::GetInstance().accessComponent<ComponentData>();
    }
};
