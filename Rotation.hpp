#pragma once
#include "ComponentData.hpp"

struct Rotation : ComponentData
{
    float a;
    float b;
    float c;

    static constexpr UniqueId GetUniqueId() { return UniqueId::Rotation; }
};
