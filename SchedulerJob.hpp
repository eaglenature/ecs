#pragma once

#include "ISchedulerJob.hpp"

class SchedulerJob : public ISchedulerJob
{
public:
    SchedulerJob(const std::string& name) : name(name)
    {}

    bool hasPredecessors() const override
    {
        return predecessors > 0u;
    }

    void addPredecessor() override
    {
        ++predecessors;
    }

    void removePredecessor() override
    {
        if (predecessors) --predecessors;
    }

    bool hasNext() const override
    {
        return next != nullptr;
    }

    void attachNext(ISchedulerJob* job) override
    {        
        next = job;
        next->addPredecessor();
    }

    ISchedulerJob* takeNext() override
    {
        next->removePredecessor();
        return next;
    }

    std::string getName() const override
    {
        return name;
    }

private:
    std::size_t predecessors = 0u;
    ISchedulerJob* next = nullptr;

    //@remove
    std::string name;
};
