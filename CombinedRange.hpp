#pragma once
#include <tuple>
#include <functional>

template <typename... Iterators>
struct CombinedIterator
{
public:
    CombinedIterator(Iterators... iterators)
        : iterators(iterators...)
    {}

    CombinedIterator& operator++()
    {
        incrementImpl(std::index_sequence_for<Iterators...>{});
        return *this;
    }

    auto operator*()
    {
        return dereferenceImpl(std::index_sequence_for<Iterators...>{});
    }

    bool operator==(const CombinedIterator& right) const
    {
        return iterators == right.iterators;
    }

    bool operator!=(const CombinedIterator& right) const
    {
        return not (*this == right);
    }

private:
    template <std::size_t ...I>
    void incrementImpl(std::index_sequence<I...>)
    {
         ((++std::get<I>(iterators)), ...);
    }

    template <std::size_t ...I>
    auto dereferenceImpl(std::index_sequence<I...>)
    {
         return std::make_tuple(std::ref(*std::get<I>(iterators))...);
    }

    std::tuple<Iterators...> iterators;
};

template <typename... Iterators>
auto makeCombinedIterator(Iterators... iterators)
{
    return CombinedIterator<Iterators...>(iterators...);
}

template <typename... Ranges>
struct CombinedRange
{
public:
    CombinedRange(Ranges&... ranges)
        : ranges(ranges...)
    {}

    auto begin()
    {
        return beginImpl(std::index_sequence_for<Ranges...>{});
    }

    auto end()
    {
        return endImpl(std::index_sequence_for<Ranges...>{});
    }

private:
    template <std::size_t ...I>
    auto beginImpl(std::index_sequence<I...>)
    {
        return makeCombinedIterator(std::get<I>(ranges).begin()...);
    }

    template <std::size_t ...I>
    auto endImpl(std::index_sequence<I...>)
    {
        return makeCombinedIterator(std::get<I>(ranges).end()...);
    }

    std::tuple<Ranges&...> ranges;
};

template <typename... Ranges>
auto makeCombinedRange(Ranges&... ranges)
{
    return CombinedRange<Ranges...>(ranges...);
}

