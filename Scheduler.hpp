#pragma once

#include <condition_variable>
#include <vector>

class ITaskRunner;
class ISchedulerJob;

class Scheduler
{
public:
    explicit Scheduler(ITaskRunner& taskRunner);
    void addJob(ISchedulerJob* job);
    void schedule();

private:
    void schedule(ISchedulerJob* job);
    void scheduleNext(ISchedulerJob* job);
    void waitUntilDone();
    void notifyDone();

    using SchedulerJobs = std::vector<ISchedulerJob*>;

    SchedulerJobs jobs;
    ITaskRunner& taskRunner;
    std::mutex m;
    std::condition_variable conditionDone;
    bool done = false;
};
