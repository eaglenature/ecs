#pragma once

#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>
#include "ITaskRunner.hpp"

class ThreadPool : public ITaskRunner
{
public:
    ThreadPool(std::size_t numOfThreads, std::size_t queueLimit);
    ThreadPool(const ThreadPool&) = delete;
    ThreadPool(ThreadPool&&) = delete;
    ~ThreadPool() override;

    void submit(Task&& task) override;

private:
    struct Job
    {
        Task task;
    };

    struct ThreadSafeQueue
    {
        explicit ThreadSafeQueue(std::size_t queueLimit);
        void push(Job&& job);
        Job pop();

        using Jobs = std::queue<Job>;
        Jobs jobs;
        std::mutex m;
        std::condition_variable conditionPush;
        std::condition_variable conditionPop;
        std::size_t queueLimit;
    };

    void createWorker();

    using Workers = std::vector<std::thread>;
    Workers workers;
    ThreadSafeQueue queue;
};
