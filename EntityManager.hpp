#pragma once
#include <memory>
#include <unordered_map>
#include "ComponentData.hpp"

class EntityManager
{
public:
    static EntityManager& GetInstance()
    {
        static EntityManager instance;
        return instance;
    }

    template <typename ComponentData>
    void addComponent()
    {
        auto it = entities.find(ComponentData::GetUniqueId());
        if (it == entities.end())
        {
            entities.emplace(ComponentData::GetUniqueId(), ComponentDataStore<ComponentData>::CreateComponent());
        }
    }

    template <typename ComponentData>
    ComponentDataArray<ComponentData>& accessComponent()
    {
        auto& component = *entities.find(ComponentData::GetUniqueId())->second;
        return ComponentDataStore<ComponentData>::GetComponentDataArray(component);
    }

    template <typename ComponentData>
    ComponentDataArray<ComponentData> const& accessComponent() const
    {
        auto const& component = *entities.find(ComponentData::GetUniqueId())->second;
        return ComponentDataStore<ComponentData>::GetComponentDataArray(component);
    }

private:
    EntityManager() = default;
    EntityManager(const EntityManager&) = delete;

    using Entities = std::unordered_map<ComponentData::UniqueId, Component::StoredType>;
    Entities entities;
};
