#include <gtest/gtest.h>
#include <chrono>
#include <cmath>
#include "ThreadPool.hpp"

namespace
{
struct ThreadPoolTest : ::testing::Test
{
    void timeConsumingProcedure()
    {
        using namespace std::chrono_literals;
        auto counter = 100u;
        while (counter--)
        {
            auto i = counter * sqrt(counter) - counter;
            i = i*i;
            std::this_thread::sleep_for(1ms);
        }
    }
};
}

TEST_F(ThreadPoolTest, createThreadPool)
{
    const auto NUMBER_OF_WORKERS = 4u;
    const auto TASK_QUEUE_LIMIT = 100u;

    ThreadPool threadPool{NUMBER_OF_WORKERS, TASK_QUEUE_LIMIT};

    auto taskCounter = 12u;
    while (taskCounter--) threadPool.submit([this]() { timeConsumingProcedure(); });
}


TEST_F(ThreadPoolTest, createOneThread)
{
    std::thread worker([this](){ timeConsumingProcedure(); });
    worker.join();
}

TEST_F(ThreadPoolTest, createManyThreads)
{
    std::vector<std::thread> threads;

    auto taskCounter = 4u;
    while(taskCounter--) threads.emplace_back([this](){ timeConsumingProcedure(); });

    for (auto& thd : threads) thd.join();
}
