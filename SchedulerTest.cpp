#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <chrono>
#include "ThreadPool.hpp"
#include "SchedulerJob.hpp"
#include "Scheduler.hpp"
using namespace ::testing;

namespace
{
struct TaskRunnerMockable : ITaskRunner
{
    virtual void mocked_submit(ITaskRunner::Task& task) = 0;
    void submit(ITaskRunner::Task&& task) override
    {
        mocked_submit(task);
    }
};
}

struct TaskRunnerMock : TaskRunnerMockable
{
    MOCK_METHOD1(mocked_submit, void(ITaskRunner::Task&));
};
struct SchedulerJobMock : ISchedulerJob
{
    MOCK_METHOD0(execute, void());

    MOCK_CONST_METHOD0(hasPredecessors, bool());
    MOCK_METHOD0(addPredecessor, void());
    MOCK_METHOD0(removePredecessor, void());
    MOCK_CONST_METHOD0(hasNext, bool());
    MOCK_METHOD1(attachNext, void(ISchedulerJob*));
    MOCK_METHOD0(takeNext, ISchedulerJob*());
    MOCK_CONST_METHOD0(getName, std::string());
};

namespace
{

struct JobA : SchedulerJob
{
    using SchedulerJob::SchedulerJob;
    void execute()
    {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(10ms);
        std::cout << __PRETTY_FUNCTION__ << " " << getName() << std::endl;
        std::this_thread::sleep_for(40ms);
    }
};

struct JobB : SchedulerJob
{
    using SchedulerJob::SchedulerJob;
    void execute()
    {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(10ms);
        std::cout << __PRETTY_FUNCTION__ << " " << getName() << std::endl;
        std::this_thread::sleep_for(50ms);
    }
};

struct JobC : SchedulerJob
{
    using SchedulerJob::SchedulerJob;
    void execute()
    {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(30ms);
        std::cout << __PRETTY_FUNCTION__ << " " << getName() << std::endl;
        std::this_thread::sleep_for(10ms);
    }
};

struct JobD : SchedulerJob
{
    using SchedulerJob::SchedulerJob;
    void execute()
    {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(30ms);
        std::cout << __PRETTY_FUNCTION__ << " " << getName() << std::endl;
        std::this_thread::sleep_for(10ms);
    }
};

struct JobE : SchedulerJob
{
    using SchedulerJob::SchedulerJob;
    void execute()
    {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(30ms);
        std::cout << __PRETTY_FUNCTION__ << " " << getName() << std::endl;
        std::this_thread::sleep_for(10ms);
    }
};

struct SchedulerTest : Test
{
    StrictMock<TaskRunnerMock> threadPoolMock;
};
}

TEST_F(SchedulerTest, createScheduler)
{
    JobA a{"A"};
    JobB b{"B"};
    JobC c{"C"};
    JobD d{"D"};
    JobE e{"E"};

    //  A ----+
    //        |
    //  B ----C----+
    //             |
    //  D--------- E --- DONE
    //

    a.attachNext(&c);
    b.attachNext(&c);
    c.attachNext(&e);
    d.attachNext(&e);

    ThreadPool threadPool{4u, 10u};
    Scheduler scheduler{threadPool};

    scheduler.addJob(&a);
    scheduler.addJob(&b);
    scheduler.addJob(&c);
    scheduler.addJob(&d);
    scheduler.addJob(&e);

    scheduler.schedule();
}
